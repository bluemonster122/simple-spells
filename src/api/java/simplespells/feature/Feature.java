package simplespells.feature;

public @interface Feature {
    String name();
}
