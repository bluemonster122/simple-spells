package simplespells;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;
import simplespells.proxy.IProxy;
import simplespells.reference.Refs;

import static simplespells.reference.Refs.MOD_ID;

@Mod(modid = Refs.MOD_ID, name = Refs.MOD_NAME, version = Refs.VERSION, guiFactory = Refs.GUI_FACTORY_CLASS)
public class SimpleSpells {
    @Mod.Instance(value = MOD_ID)
    public static SimpleSpells INSTANCE = null;
    @SidedProxy(clientSide = Refs.CLIENT_PROXY_CLASS, serverSide = Refs.SERVER_PROXY_CLASS)
    public static IProxy proxy;
    private Logger logger;

    public static Logger getLogger() {
        return INSTANCE.logger;
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();

        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(ModRegistry.EventHandler.class);
        ModConfig.loadConfigs();

        proxy.preInit();
    }
}
