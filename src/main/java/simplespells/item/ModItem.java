package simplespells.item;

import net.minecraft.item.Item;
import simplespells.reference.Refs;

public class ModItem extends Item {
    private String name;

    public ModItem() {
    }

    @Override
    public Item setUnlocalizedName(String name) {
        Refs.setRL(this, name);
        return super.setUnlocalizedName(Refs.MOD_ID + ":" + name);
    }
}
