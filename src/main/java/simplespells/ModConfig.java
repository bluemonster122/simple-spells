package simplespells;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import simplespells.reference.Refs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static simplespells.reference.Refs.MOD_ID;

public class ModConfig {
    public static boolean prop;
    public static int prop2;
    private static Configuration config;

    private ModConfig() {
    }

    public static void loadConfigs() {
        if (config == null) {
            config = new Configuration(new File(Loader.instance().getConfigDir(), MOD_ID + ".cfg"));
        }
        config.load();
        syncConfigs();
    }

    public static void syncConfigs() {
        String lang = Refs.LANG_CONFIG_CORE;

        // Setup Properties
        Property pProp = config.get(Refs.CATEGORY_CORE, "prop", false, "Config Boolean (prop)").setLanguageKey(lang + ".prop");
        Property pProp2 = config.get(Refs.CATEGORY_CORE, "prop2", 5, "Config Integer (prop2)", 1, 100).setLanguageKey(lang + ".prop2");

        // Order them
        List<String> order = new ArrayList<>();
        order.add(pProp.getName());
        order.add(pProp2.getName());
        config.setCategoryPropertyOrder(Refs.CATEGORY_CORE, order);

        // Read their values and ensure they are within bounds if necessary
        prop = pProp.getBoolean(false);
        prop2 = pProp2.getInt(5);
        if (1 > prop2 || prop2 > 100) {
            prop2 = 5;
        }

        // Write values in case values were outside of boundaries
        pProp.set(prop);
        pProp2.set(prop2);

        // Trigger save if something has changed
        if (config.hasChanged()) {
            config.save();
        }
    }

    public static Configuration getConfig() {
        return config;
    }

    @SideOnly(Side.CLIENT)
    public static class EventHandler {
        @SubscribeEvent
        public static void onConfigsChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
            if (event.getModID().equals(MOD_ID)) {
                syncConfigs();
            }
        }
    }
}
