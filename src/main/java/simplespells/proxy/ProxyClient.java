package simplespells.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import simplespells.ModConfig;
import simplespells.ModRegistry;

@SideOnly(Side.CLIENT)
public class ProxyClient implements IProxy {
    @Override
    public void preInit() {
        MinecraftForge.EVENT_BUS.register(ModConfig.EventHandler.class);
        MinecraftForge.EVENT_BUS.register(ModRegistry.EventHandlerClient.class);
    }
}
